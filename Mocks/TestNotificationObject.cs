﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acrotech.PortableViewModel.Test.Mocks
{
    public class TestNotificationObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = null;

        public TestNotificationObject(string initialValue = default(string))
        {
            propText = initialValue;
        }

        public bool OnTextChangedOccurred { get; private set; }
        public string PreviousTextValue { get; private set; }

        #region Text

        private string propText = default(string);

        public const string TextPropertyName = "Text";

        public string Text
        {
            get { return propText; }
            set { this.RaiseAndSetIfChanged(PropertyChanged, ref propText, value, TextPropertyName, OnTextChanged); }
        }

        protected virtual void OnTextChanged(string previousValue)
        {
            OnTextChangedOccurred = true;
            PreviousTextValue = previousValue;
        }

        #endregion

        public void Reset()
        {
            OnTextChangedOccurred = false;
            PreviousTextValue = null;
        }
    }
}
