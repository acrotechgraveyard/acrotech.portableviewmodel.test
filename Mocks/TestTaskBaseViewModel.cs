﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acrotech.PortableViewModel.Test.Mocks
{
    public class TestTaskBaseViewModel : TaskBaseViewModel
    {
        public TestTaskBaseViewModel(TaskScheduler scheduler, string initialValue = default(string))
            : base(scheduler)
        {
            propText = initialValue;
        }

        public TestTaskBaseViewModel(string initialValue = default(string))
        {
            propText = initialValue;
        }

        #region Text

        private string propText = default(string);

        public const string TextPropertyName = "Text";

        public string Text
        {
            get { return propText; }
            set { RaiseAndSetIfChanged(ref propText, value, TextPropertyName); }
        }

        #endregion

        public Task PerformRunOnUiThread(Action action)
        {
            return RunOnUiThread(action);
        }

        public Task PerformRunOffUiThread(Action action)
        {
            return RunOffUiThread(action);
        }

        public void PerformOnPropertyChanged(PropertyChangedEventHandler handler, Func<PropertyChangedEventArgs> argsCreator)
        {
            OnPropertyChanged(handler, argsCreator);
        }
    }
}
