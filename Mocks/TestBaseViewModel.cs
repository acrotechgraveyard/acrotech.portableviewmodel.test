﻿// ignore warning about obsolete method
#pragma warning disable 0618

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acrotech.PortableViewModel.Test.Mocks
{
    public class TestBaseViewModel : BaseViewModel
    {
        public TestBaseViewModel(string initialValue = default(string))
        {
            propText = initialValue;
        }

        public bool OnTextChangedOccurred { get; private set; }
        public string PreviousTextValue { get; private set; }

        #region Text

        private string propText = default(string);

        public const string TextPropertyName = "Text";

        public string Text
        {
            get { return propText; }
            set { RaiseAndSetIfChanged(ref propText, value, TextPropertyName, OnTextChanged); }
        }

        protected virtual void OnTextChanged(string previousValue)
        {
            OnTextChangedOccurred = true;
            PreviousTextValue = previousValue;
        }

        #endregion

        #region PerformOnPropertyChanged

        public void PerformOnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(propertyName);
        }

        public void PerformOnPropertyChanged(PropertyChangedEventArgs args)
        {
            OnPropertyChanged(args);
        }

        public void PerformOnPropertyChanged(Func<PropertyChangedEventArgs> argsCreator)
        {
            OnPropertyChanged(argsCreator);
        }

        public void PerformOnPropertyChanged(PropertyChangedEventHandler handler, Func<PropertyChangedEventArgs> argsCreator)
        {
            OnPropertyChanged(handler, argsCreator);
        }

        #endregion

        #region PerformRaiseAndSetIfChanged

        public T PerformRaiseAndSetIfChanged<T>(ref T backingField, T newValue, string propertyName, Action<T> onChanged = null)
        {
            return RaiseAndSetIfChanged(ref backingField, newValue, propertyName, onChanged);
        }

        public T PerformRaiseAndSetIfChanged<T>(ref T backingField, T newValue, PropertyChangedEventArgs args, Action<T> onChanged = null)
        {
            return RaiseAndSetIfChanged(ref backingField, newValue, args, onChanged);
        }

        public T PerformRaiseAndSetIfChanged<T>(ref T backingField, T newValue, Func<PropertyChangedEventArgs> argsCreator, Action<T> onChanged = null)
        {
            return RaiseAndSetIfChanged(ref backingField, newValue, argsCreator, onChanged);
        }

        public T PerformRaiseAndSetIfChanged<T>(ref T backingField, T newValue, string propertyName, Action onChanged)
        {
            return RaiseAndSetIfChanged(ref backingField, newValue, propertyName, onChanged);
        }

        #endregion
    }
}
