﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.PortableViewModel.Test.Mocks;
using System.ComponentModel;

namespace Acrotech.PortableViewModel.Test
{
    [TestClass]
    public class BaseViewModelTest
    {
        const string OldValue = "OldValue";
        const string NewValue = "NewValue";

        [TestMethod]
        public void TextValueNotUpdatedTest()
        {
            var vm = new TestBaseViewModel(OldValue);

            vm.Text = OldValue;

            Assert.AreEqual(false, vm.OnTextChangedOccurred);
            Assert.AreEqual(OldValue, vm.Text);
        }

        [TestMethod]
        public void TextValueUpdatedTest()
        {
            var vm = new TestBaseViewModel(OldValue);

            vm.Text = NewValue;

            Assert.AreEqual(true, vm.OnTextChangedOccurred);
            Assert.AreEqual(OldValue, vm.PreviousTextValue);
            Assert.AreEqual(NewValue, vm.Text);
        }

        [TestMethod]
        public void TextValueUpdatedEventHandlerCalledTest()
        {
            var vm = new TestBaseViewModel(OldValue);

            var handlerCalled = false;
            PropertyChangedEventArgs handlerArgs = null;

            vm.PropertyChanged += (s, e) => { handlerCalled = true; handlerArgs = e; };

            vm.Text = NewValue;

            Assert.AreEqual(true, handlerCalled);
            Assert.AreEqual(TestBaseViewModel.TextPropertyName, handlerArgs.PropertyName);
        }

        [TestMethod]
        public void OnPropertyChangedWithPropertyNameTest()
        {
            var vm = new TestBaseViewModel();

            var handlerCalled = false;
            PropertyChangedEventArgs handlerArgs = null;

            vm.PropertyChanged += (s, e) => { handlerCalled = true; handlerArgs = e; };

            vm.PerformOnPropertyChanged(NewValue);

            Assert.AreEqual(true, handlerCalled);
            Assert.AreEqual(NewValue, handlerArgs.PropertyName);
        }

        [TestMethod]
        public void OnPropertyChangedWithArgsTest()
        {
            var vm = new TestBaseViewModel();

            var args = new PropertyChangedEventArgs(NewValue);

            var handlerCalled = false;
            PropertyChangedEventArgs handlerArgs = null;

            vm.PropertyChanged += (s, e) => { handlerCalled = true; handlerArgs = e; };

            vm.PerformOnPropertyChanged(args);

            Assert.AreEqual(true, handlerCalled);
            Assert.AreEqual(args, handlerArgs);
            Assert.AreEqual(NewValue, handlerArgs.PropertyName);
        }

        [TestMethod]
        public void OnPropertyChangedWithArgsCreatorTest()
        {
            var vm = new TestBaseViewModel();

            var args = new PropertyChangedEventArgs(NewValue);
            var argsCreated = false;

            var handlerCalled = false;
            PropertyChangedEventArgs handlerArgs = null;

            vm.PropertyChanged += (s, e) => { handlerCalled = true; handlerArgs = e; };

            vm.PerformOnPropertyChanged(() => { argsCreated = true; return args; });

            Assert.AreEqual(true, handlerCalled);
            Assert.AreEqual(true, argsCreated);
            Assert.AreEqual(args, handlerArgs);
            Assert.AreEqual(NewValue, handlerArgs.PropertyName);
        }

        [TestMethod]
        public void OnPropertyChangedWithArgsCreatorAndNullHandlerTest()
        {
            var vm = new TestBaseViewModel();

            var args = new PropertyChangedEventArgs(NewValue);
            var argsCreated = false;

            vm.PerformOnPropertyChanged(null, () => { argsCreated = true; return args; });

            Assert.AreEqual(false, argsCreated);
        }

        [TestMethod]
        public void OnPropertyChangedWithArgsCreatorAndNonNullHandlerTest()
        {
            var vm = new TestBaseViewModel();

            var args = new PropertyChangedEventArgs(NewValue);
            var argsCreated = false;

            var handlerCalled = false;
            PropertyChangedEventArgs handlerArgs = null;

            PropertyChangedEventHandler handler = (s, e) => { handlerCalled = true; handlerArgs = e; };

            vm.PerformOnPropertyChanged(handler, () => { argsCreated = true; return args; });

            Assert.AreEqual(true, handlerCalled);
            Assert.AreEqual(true, argsCreated);
            Assert.AreEqual(args, handlerArgs);
            Assert.AreEqual(NewValue, handlerArgs.PropertyName);
        }

        [TestMethod]
        public void RaiseAndSetIfChangedWithPropertyNameTest()
        {
            var vm = new TestBaseViewModel();

            string value = OldValue;
            var onChangedCalled = false;

            vm.PerformRaiseAndSetIfChanged(ref value, NewValue, TestBaseViewModel.TextPropertyName, _ => onChangedCalled = true);

            Assert.AreEqual(true, onChangedCalled);
        }

        [TestMethod]
        public void RaiseAndSetIfChangedWithArgsTest()
        {
            var vm = new TestBaseViewModel();

            string value = OldValue;
            var onChangedCalled = false;

            vm.PerformRaiseAndSetIfChanged(ref value, NewValue, new PropertyChangedEventArgs(TestBaseViewModel.TextPropertyName), _ => onChangedCalled = true);

            Assert.AreEqual(true, onChangedCalled);
        }

        [TestMethod]
        public void RaiseAndSetIfChangedWithArgsCreatorTest()
        {
            var vm = new TestBaseViewModel();

            string value = OldValue;
            var onChangedCalled = false;

            vm.PerformRaiseAndSetIfChanged(ref value, NewValue, () => new PropertyChangedEventArgs(TestBaseViewModel.TextPropertyName), _ => onChangedCalled = true);

            Assert.AreEqual(true, onChangedCalled);
        }

        [TestMethod]
        public void RaiseAndSetIfChangedWithLegacyPropertyNameTest()
        {
            var vm = new TestBaseViewModel();

            string value = OldValue;
            var onChangedCalled = false;

            vm.PerformRaiseAndSetIfChanged(ref value, NewValue, TestBaseViewModel.TextPropertyName, () => onChangedCalled = true);

            Assert.AreEqual(true, onChangedCalled);
        }
    }
}
