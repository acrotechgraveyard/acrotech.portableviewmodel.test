﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.PortableViewModel.Test.Mocks;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;

namespace Acrotech.PortableViewModel.Test
{
    [TestClass]
    public class TaskBaseViewModelTest
    {
        public const int DefaultDelay = 20;

        [TestMethod]
        public void EmptyCompletedTaskTest()
        {
            Assert.IsNotNull(TaskBaseViewModel.EmptyCompletedTask);
        }

        [TestMethod]
        public void CreateEmptyCompletedTaskTest()
        {
            var task = TaskBaseViewModel.CreateEmptyCompletedTask<object>();

            Assert.IsNotNull(task);
            Assert.IsTrue(task.IsCompleted);
            Assert.IsFalse(task.IsCanceled);
            Assert.IsFalse(task.IsFaulted);
            Assert.AreEqual(TaskStatus.RanToCompletion, task.Status);
        }

        [TestMethod]
        public void CreateEmptyCompletedTaskWithResultTest()
        {
            const string Result = "Test";

            var task = TaskBaseViewModel.CreateEmptyCompletedTask<object>(Result);

            Assert.IsNotNull(task);
            Assert.AreEqual(Result, task.Result);
        }

        [TestMethod]
        public void SchedulerNullTest()
        {
            var vm = new TestTaskBaseViewModel(scheduler: null);

            Assert.IsNull(vm.UIScheduler);
        }

        [TestMethod]
        public void SchedulerNonNullTest()
        {
            var scheduler = new TestTaskScheduler();

            var vm = new TestTaskBaseViewModel(scheduler);

            Assert.AreEqual(scheduler, vm.UIScheduler);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void SchedulerFromCurrentNullSynchronizationContextTest()
        {
            var vm = new TestTaskBaseViewModel();
        }

        [TestMethod]
        public void SchedulerFromCurrentNonNullSynchronizationContextTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            var vm = new TestTaskBaseViewModel();

            Assert.IsNotNull(vm.UIScheduler);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RunOnUiThreadWithNullSchedulerTest()
        {
            var vm = new TestTaskBaseViewModel(scheduler: null);

            vm.PerformRunOnUiThread(null);
        }

        [TestMethod]
        public void RunOnUiThreadWithNullActionTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            var vm = new TestTaskBaseViewModel();

            var task = vm.PerformRunOnUiThread(null);

            Assert.IsNotNull(task);
            Assert.AreEqual(TaskBaseViewModel.EmptyCompletedTask, task);
        }

        [TestMethod]
        public void RunOnUiThreadFromUiThreadTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            var vm = new TestTaskBaseViewModel();

            var actionCalled = false;
            TaskScheduler actionScheduler = null;

            var task = vm.PerformRunOnUiThread(() => { actionCalled = true; actionScheduler = TaskScheduler.Current; Task.Delay(DefaultDelay).Wait(); });

            Assert.IsNotNull(task);
            Assert.IsFalse(task.IsCompleted);
            Assert.IsFalse(actionCalled);

            task.Wait();

            Assert.IsTrue(task.IsCompleted);
            Assert.IsTrue(actionCalled);
            Assert.AreEqual(vm.UIScheduler, actionScheduler);
        }

        [TestMethod]
        public void RunOnUiThreadFromNonUiThreadTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            var vm = new TestTaskBaseViewModel();

            var actionCalled = false;
            TaskScheduler actionScheduler = null;

            Task.Factory.StartNew(() =>
            {
                Assert.AreNotEqual(TaskScheduler.Current.Id, vm.UIScheduler.Id);

                var task = vm.PerformRunOnUiThread(() => { actionCalled = true; actionScheduler = TaskScheduler.Current; Task.Delay(DefaultDelay).Wait(); });

                Assert.IsNotNull(task);
                Assert.IsFalse(task.IsCompleted);
                Assert.IsFalse(actionCalled);

                task.Wait();

                Assert.IsTrue(task.IsCompleted);
                Assert.IsTrue(actionCalled);
                Assert.AreEqual(vm.UIScheduler, actionScheduler);
            }).Wait();
        }

        [TestMethod]
        public void RunOffUiThreadWithNullSchedulerTest()
        {
            var vm = new TestTaskBaseViewModel(scheduler: null);

            var task = vm.PerformRunOffUiThread(null);

            Assert.IsNotNull(task);
            Assert.AreEqual(TaskBaseViewModel.EmptyCompletedTask, task);
        }

        [TestMethod]
        public void RunOffUiThreadWithNullActionTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            var vm = new TestTaskBaseViewModel();

            var task = vm.PerformRunOffUiThread(null);

            Assert.IsNotNull(task);
            Assert.AreEqual(TaskBaseViewModel.EmptyCompletedTask, task);
        }

        [TestMethod]
        public void RunOffUiThreadFromUiThreadTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            var vm = new TestTaskBaseViewModel();

            var actionCalled = false;
            TaskScheduler actionScheduler = null;

            var task = vm.PerformRunOffUiThread(() => { actionCalled = true; actionScheduler = TaskScheduler.Current; Task.Delay(DefaultDelay).Wait(); });

            Assert.IsNotNull(task);
            Assert.IsFalse(task.IsCompleted);
            Assert.IsFalse(actionCalled);

            task.Wait();

            Assert.IsTrue(task.IsCompleted);
            Assert.IsTrue(actionCalled);
            Assert.AreNotEqual(vm.UIScheduler, actionScheduler);
        }

        [TestMethod]
        public void RunOffUiThreadFromNonUiThreadTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            var vm = new TestTaskBaseViewModel();

            var actionCalled = false;
            TaskScheduler actionScheduler = null;

            Task.Factory.StartNew(() =>
            {
                Assert.AreNotEqual(TaskScheduler.Current.Id, vm.UIScheduler.Id);

                var task = vm.PerformRunOffUiThread(() => { actionCalled = true; actionScheduler = TaskScheduler.Current; Task.Delay(DefaultDelay).Wait(); });

                Assert.IsNotNull(task);
                Assert.IsFalse(task.IsCompleted);
                Assert.IsFalse(actionCalled);

                task.Wait();

                Assert.IsTrue(task.IsCompleted);
                Assert.IsTrue(actionCalled);
                Assert.AreNotEqual(vm.UIScheduler, actionScheduler);
            }).Wait();
        }

        [TestMethod]
        public void StartTaskWithNullActionTest()
        {
            var task = TaskBaseViewModel.StartTask(null);

            Assert.IsNotNull(task);
            Assert.AreEqual(TaskBaseViewModel.EmptyCompletedTask, task);
        }

        [TestMethod]
        public void StartTaskWithNonNullActionTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            var actionCalled = false;
            TaskScheduler actionScheduler = null;
            var creationOptions = TaskCreationOptions.PreferFairness;
            var scheduler = TaskScheduler.FromCurrentSynchronizationContext();

            var task = TaskBaseViewModel.StartTask(() => { actionCalled = true; actionScheduler = TaskScheduler.Current; }, CancellationToken.None, creationOptions, scheduler);

            Assert.IsNotNull(task);

            task.Wait();

            Assert.IsTrue(actionCalled);
            Assert.AreEqual(scheduler, actionScheduler);
            Assert.AreEqual(creationOptions, task.CreationOptions);
        }

        [TestMethod]
        public void StartTaskThenCancelTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            var actionCalled = false;
            TaskScheduler actionScheduler = null;
            var cts = new CancellationTokenSource();
            var creationOptions = TaskCreationOptions.None;
            var scheduler = TaskScheduler.FromCurrentSynchronizationContext();

            var task = TaskBaseViewModel.StartTask(() =>
            {
                actionCalled = true; 
                actionScheduler = TaskScheduler.Current; 

                // initial work
                Task.Delay(DefaultDelay).Wait();

                // don't continue if cancelled
                cts.Token.ThrowIfCancellationRequested();

                // secondary work if not cancelled
                Task.Delay(DefaultDelay).Wait();
            }, cts.Token, creationOptions, scheduler);

            Assert.IsNotNull(task);

            Assert.IsFalse(task.Wait(DefaultDelay / 2));
            
            try
            {
                cts.Cancel();
                task.Wait();
            }
            catch (AggregateException e)
            {
                Assert.AreEqual(1, e.InnerExceptions.Count);
                Assert.IsInstanceOfType(e.InnerException, typeof(OperationCanceledException));
            }

            Assert.IsTrue(actionCalled);
            Assert.AreEqual(scheduler, actionScheduler);
            Assert.IsTrue(task.IsCanceled);
        }

        [TestMethod]
        public void OnPropertyChangedWithNullHandlerTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            var vm = new TestTaskBaseViewModel();

            var args = new PropertyChangedEventArgs(TestTaskBaseViewModel.TextPropertyName);
            var argsCreated = false;

            vm.PerformOnPropertyChanged(null, () => { argsCreated = true; return args; });

            Assert.IsFalse(argsCreated);
        }

        [TestMethod]
        public void OnPropertyChangedFromNonUiThreadTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            var vm = new TestTaskBaseViewModel();

            var args = new PropertyChangedEventArgs(TestTaskBaseViewModel.TextPropertyName);
            var argsCreated = false;

            var handlerCalled = false;
            PropertyChangedEventArgs handlerArgs = null;
            TaskScheduler handlerScheduler = null;

            var waitHandle = new ManualResetEventSlim();

            PropertyChangedEventHandler handler = (s, e) => { handlerCalled = true; handlerArgs = e; handlerScheduler = TaskScheduler.Current; waitHandle.Set(); };

            vm.PerformOnPropertyChanged(handler, () => { argsCreated = true; return args; });

            // we need to wait here because we don't have any other way of syncronizing with proeprty changed events (since they are async)
            waitHandle.Wait();

            Assert.IsTrue(argsCreated);
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual(args, handlerArgs);
            Assert.AreEqual(vm.UIScheduler, handlerScheduler);
        }

        [TestMethod]
        public void OnPropertyChangedFromUiThreadTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            var vm = new TestTaskBaseViewModel();

            var args = new PropertyChangedEventArgs(TestTaskBaseViewModel.TextPropertyName);
            var argsCreated = false;

            var handlerCalled = false;
            PropertyChangedEventArgs handlerArgs = null;
            TaskScheduler handlerScheduler = null;

            var waitHandle = new ManualResetEventSlim();

            PropertyChangedEventHandler handler = (s, e) => { handlerCalled = true; handlerArgs = e; handlerScheduler = TaskScheduler.Current; waitHandle.Set(); };

            var task = vm.PerformRunOnUiThread(() => vm.PerformOnPropertyChanged(handler, () => { argsCreated = true; return args; }));

            // we need to wait here because we don't have any other way of syncronizing with proeprty changed events (since they are async)
            waitHandle.Wait();

            Assert.IsTrue(argsCreated);
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual(args, handlerArgs);
            Assert.AreEqual(vm.UIScheduler, handlerScheduler);
        }

        class TestTaskScheduler : TaskScheduler
        {
            protected override System.Collections.Generic.IEnumerable<Task> GetScheduledTasks()
            {
                throw new NotImplementedException();
            }

            protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
            {
                throw new NotImplementedException();
            }

            protected override void QueueTask(Task task)
            {
                throw new NotImplementedException();
            }
        }
    }
}
