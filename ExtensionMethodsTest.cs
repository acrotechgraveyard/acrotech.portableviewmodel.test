﻿// ignore warning about obsolete method
#pragma warning disable 0618

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;
using System.ComponentModel;
using Acrotech.PortableViewModel.Test.Mocks;

namespace Acrotech.PortableViewModel.Test
{
    [TestClass]
    public class ExtensionMethodsTest
    {
        const string OldValue = "OldValue";
        const string NewValue = "NewValue";

        [TestMethod]
        public void RaiseAndSetIfChangedNullSourceTest()
        {
            INotifyPropertyChanged obj = null;

            string field = OldValue;
            var onChangedCalled = false;
            var handlerCalled = false;

            var result = obj.RaiseAndSetIfChanged(null, ref field, NewValue, (PropertyChangedEventArgs)null, null);

            Assert.AreEqual(NewValue, result);
            Assert.AreEqual(OldValue, field);
            Assert.IsFalse(onChangedCalled);
            Assert.IsFalse(handlerCalled);
        }

        [TestMethod]
        public void RaiseAndSetIfChangedNoValueChangeTest()
        {
            INotifyPropertyChanged obj = new TestNotificationObject();

            string field = NewValue;
            var onChangedCalled = false;
            var handlerCalled = false;

            var result = obj.RaiseAndSetIfChanged(null, ref field, NewValue, (PropertyChangedEventArgs)null, null);

            Assert.AreEqual(NewValue, result);
            Assert.AreEqual(NewValue, field);
            Assert.IsFalse(onChangedCalled);
            Assert.IsFalse(handlerCalled);
        }

        [TestMethod]
        public void RaiseAndSetIfChangedNullHandlerTest()
        {
            INotifyPropertyChanged obj = new TestNotificationObject();

            string field = OldValue;
            string onChangedValue = null;
            var onChangedCalled = false;

            Action<string> onChanged = (x) => { onChangedCalled = true; onChangedValue = x; };

            var result = obj.RaiseAndSetIfChanged(null, ref field, NewValue, (PropertyChangedEventArgs)null, onChanged);

            Assert.AreEqual(NewValue, result);
            Assert.AreEqual(NewValue, field);
            Assert.IsTrue(onChangedCalled);
            Assert.AreEqual(OldValue, onChangedValue);
        }

        [TestMethod]
        public void RaiseAndSetIfChangedNullOnChangedTest()
        {
            INotifyPropertyChanged obj = new TestNotificationObject();

            string field = OldValue;
            var handlerCalled = false;
            PropertyChangedEventArgs handlerArgs = null;
            var args = new PropertyChangedEventArgs(TestNotificationObject.TextPropertyName);

            PropertyChangedEventHandler handler = (s, e) => { handlerCalled = true; handlerArgs = e; };

            var result = obj.RaiseAndSetIfChanged(handler, ref field, NewValue, () => args, null);

            Assert.AreEqual(NewValue, result);
            Assert.AreEqual(NewValue, field);
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual(args, handlerArgs);
        }

        [TestMethod]
        public void RaiseAndSetIfChangedNullArgsCreatorTest()
        {
            INotifyPropertyChanged obj = new TestNotificationObject();

            string field = OldValue;
            string onChangedValue = null;
            var onChangedCalled = false;
            var handlerCalled = false;
            PropertyChangedEventArgs handlerArgs = null;
            var args = new PropertyChangedEventArgs(TestNotificationObject.TextPropertyName);

            PropertyChangedEventHandler handler = (s, e) => { handlerCalled = true; handlerArgs = e; };
            Action<string> onChanged = (x) => { onChangedCalled = true; onChangedValue = x; };

            var result = obj.RaiseAndSetIfChanged(handler, ref field, NewValue, (Func<PropertyChangedEventArgs>)null, onChanged);

            Assert.AreEqual(NewValue, result);
            Assert.AreEqual(NewValue, field);
            Assert.IsTrue(onChangedCalled);
            Assert.AreEqual(OldValue, onChangedValue);
            Assert.IsTrue(handlerCalled);
            Assert.IsNull(handlerArgs);
        }

        [TestMethod]
        public void RaiseAndSetIfChangedPropertyNameTest()
        {
            INotifyPropertyChanged obj = new TestNotificationObject();

            string field = OldValue;
            string onChangedValue = null;
            var onChangedCalled = false;
            var handlerCalled = false;
            PropertyChangedEventArgs handlerArgs = null;
            var args = new PropertyChangedEventArgs(TestNotificationObject.TextPropertyName);

            PropertyChangedEventHandler handler = (s, e) => { handlerCalled = true; handlerArgs = e; };
            Action<string> onChanged = (x) => { onChangedCalled = true; onChangedValue = x; };

            var result = obj.RaiseAndSetIfChanged(handler, ref field, NewValue, TestNotificationObject.TextPropertyName, onChanged);

            Assert.AreEqual(NewValue, result);
            Assert.AreEqual(NewValue, field);
            Assert.IsTrue(onChangedCalled);
            Assert.AreEqual(OldValue, onChangedValue);
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual(args.PropertyName, handlerArgs.PropertyName);
        }

        [TestMethod]
        public void RaiseAndSetIfChangedPropertyArgsTest()
        {
            INotifyPropertyChanged obj = new TestNotificationObject();

            string field = OldValue;
            string onChangedValue = null;
            var onChangedCalled = false;
            var handlerCalled = false;
            PropertyChangedEventArgs handlerArgs = null;
            var args = new PropertyChangedEventArgs(TestNotificationObject.TextPropertyName);

            PropertyChangedEventHandler handler = (s, e) => { handlerCalled = true; handlerArgs = e; };
            Action<string> onChanged = (x) => { onChangedCalled = true; onChangedValue = x; };

            var result = obj.RaiseAndSetIfChanged(handler, ref field, NewValue, args, onChanged);

            Assert.AreEqual(NewValue, result);
            Assert.AreEqual(NewValue, field);
            Assert.IsTrue(onChangedCalled);
            Assert.AreEqual(OldValue, onChangedValue);
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual(args, handlerArgs);
        }

        [TestMethod]
        public void RaiseAndSetIfChangedLegacyOnChangedTest()
        {
            INotifyPropertyChanged obj = new TestNotificationObject();

            string field = OldValue;
            var onChangedCalled = false;
            var handlerCalled = false;
            PropertyChangedEventArgs handlerArgs = null;
            var args = new PropertyChangedEventArgs(TestNotificationObject.TextPropertyName);

            PropertyChangedEventHandler handler = (s, e) => { handlerCalled = true; handlerArgs = e; };
            Action onChanged = () => { onChangedCalled = true; };

            var result = obj.RaiseAndSetIfChanged(handler, ref field, NewValue, TestNotificationObject.TextPropertyName, onChanged);

            Assert.AreEqual(NewValue, result);
            Assert.AreEqual(NewValue, field);
            Assert.IsTrue(onChangedCalled);
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual(args.PropertyName, handlerArgs.PropertyName);
        }

        [TestMethod]
        public void RaiseAndSetIfChangedLegacyNullOnChangedTest()
        {
            INotifyPropertyChanged obj = new TestNotificationObject();

            string field = OldValue;
            var handlerCalled = false;
            PropertyChangedEventArgs handlerArgs = null;
            var args = new PropertyChangedEventArgs(TestNotificationObject.TextPropertyName);

            PropertyChangedEventHandler handler = (s, e) => { handlerCalled = true; handlerArgs = e; };

            var result = obj.RaiseAndSetIfChanged(handler, ref field, NewValue, TestNotificationObject.TextPropertyName, (Action)null);

            Assert.AreEqual(NewValue, result);
            Assert.AreEqual(NewValue, field);
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual(args.PropertyName, handlerArgs.PropertyName);
        }
    }
}
